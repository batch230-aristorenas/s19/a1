console.log("Hello World!");

/*
    1. Declare 3 variables without initialization called username,password and role.
*/
let userName = prompt("Enter your username");
let password = prompt("Enter your password");
let role = prompt("Enter your role");

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/
if((userName === "" || userName === null)||(password === "" || password === null)){
	alert("Input must not be empty");
}

else{
	switch(role){
		case "admin":
			alert("Welcome back to the class portal, admin!");
			break;
		case "teacher":
			alert("Thank you for logging in, teacher!");
			break;
		case "rookie":
			alert("Welcome to the class portal, student!");
			break;
		case "":
			alert("Input must not be empty");
			break;
		default:
			alert("Role out of range");
			break;
	}
}

/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/

function checkAverage(data1, data2, data3, data4){
	let solveAverage1 = (data1 + data2 + data3 + data4) / 4;
		
	return [data1, data2, data3, data4];

}
let data1 = 71;
let data2 = 70;
let data3 = 73;
let data4 = 74;

let average = checkAverage(data1, data2, data3, data4);
	console.log("checkAverage(" + data1 + "," + data2 + "," + data3 + "," + data4 + ")");
console.log(average);


if(average <= 74){
	console.log("Hello,student, your average is " + average + ". The Letter equivalent is F.");
}

else if(average >= 75 && average <= 79){
	console.log("Hello,student, your average is " + average + ". The Letter equivalent is D.");
}

else if(average >= 80 && average <= 84){
	console.log("Hello,student, your average is " + average + ". The Letter equivalent is C.");
}

else if(average >= 85 && average <= 89){
	console.log("Hello,student, your average is " + average + ". The Letter equivalent is B.");
}

else if(average >= 90 && average <= 95){
	console.log("Hello,student, your average is " + average + ". The Letter equivalent is A.");
}

else if(average > 96){
	console.log("Hello,student, your average is " + average + ". The Letter equivalent is A+.");
}

//(---------------------------------------------------)

function checkAverage(data1, data2, data3, data4){
	let solveAverage1 = (data1 + data2 + data3 + data4) / 4;
		
	return solveAverage1;

}
data1 = 75;
data2 = 75;
data3 = 76;
data4 = 78;


let average2 = checkAverage(data1, data2, data3, data4);
	console.log("checkAverage(" + data1 + "," + data2 + "," + data3 + "," + data4 + ")");
console.log(average2);


if(average2 <= 74){
	console.log("Hello,student, your average is " + average2 + ". The Letter equivalent is F.");
}

else if(average2 >= 75 && average2 <= 79){
	console.log("Hello,student, your average is " + average2 + ". The Letter equivalent is D.");
}

else if(average2 >= 80 && average2 <= 84){
	console.log("Hello,student, your average is " + average2 + ". The Letter equivalent is C.");
}

else if(average2 >= 85 && average2 <= 89){
	console.log("Hello,student, your average is " + average2 + ". The Letter equivalent is B.");
}

else if(average2 >= 90 && average2 <= 95){
	console.log("Hello,student, your average is " + average2 + ". The Letter equivalent is A.");
}

else if(average2 > 96){
	console.log("Hello,student, your average is " + average2 + ". The Letter equivalent is A+.");
}

//(---------------------------------------------------)


function checkAverage(data1, data2, data3, data4){
	let solveAverage1 = (data1 + data2 + data3 + data4) / 4;
		
	return solveAverage1;

}
data1 = 80;
data2 = 81;
data3 = 82;
data4 = 78;


let average3Decimal = checkAverage(data1, data2, data3, data4);
	console.log("checkAverage(" + data1 + "," + data2 + "," + data3 + "," + data4 + ")");
	let average3 = Math.round(average3Decimal);
console.log(average3);


if(average3 <= 74){
	console.log("Hello,student, your average is " + average3 + ". The Letter equivalent is F.");
}

else if(average3 >= 75 && average3 <= 79){
	console.log("Hello,student, your average is " + average3 + ". The Letter equivalent is D.");
}

else if(average3 >= 80 && average3 <= 84){
	console.log("Hello,student, your average is " + average3 + ". The Letter equivalent is C.");
}

else if(average3 >= 85 && average3 <= 89){
	console.log("Hello,student, your average is " + average3 + ". The Letter equivalent is B.");
}

else if(average3 >= 90 && average3 <= 95){
	console.log("Hello,student, your average is " + average3 + ". The Letter equivalent is A.");
}

else if(average3 > 96){
	console.log("Hello,student, your average is " + average3 + ". The Letter equivalent is A+.");
}


//(---------------------------------------------------)


function checkAverage(data1, data2, data3, data4){
	let solveAverage1 = (data1 + data2 + data3 + data4) / 4;
		
	return solveAverage1;

}
data1 = 84;
data2 = 85;
data3 = 87;
data4 = 88;


let average4Decimal = checkAverage(data1, data2, data3, data4);
	console.log("checkAverage(" + data1 + "," + data2 + "," + data3 + "," + data4 + ")");
	let average4 = Math.round(average4Decimal);
console.log(average4);


if(average4 <= 74){
	console.log("Hello,student, your average is " + average4 + ". The Letter equivalent is F.");
}

else if(average4 >= 75 && average4 <= 79){
	console.log("Hello,student, your average is " + average4 + ". The Letter equivalent is D.");
}

else if(average4 >= 80 && average4 <= 84){
	console.log("Hello,student, your average is " + average4 + ". The Letter equivalent is C.");
}

else if(average4 >= 85 && average4 <= 89){
	console.log("Hello,student, your average is " + average4 + ". The Letter equivalent is B.");
}

else if(average4 >= 90 && average4 <= 95){
	console.log("Hello,student, your average is " + average4 + ". The Letter equivalent is A.");
}

else if(average4 > 96){
	console.log("Hello,student, your average is " + average4 + ". The Letter equivalent is A+.");
}


//(---------------------------------------------------)


function checkAverage(data1, data2, data3, data4){
	let solveAverage1 = (data1 + data2 + data3 + data4) / 4;
		
	return solveAverage1;

}
data1 = 89;
data2 = 90;
data3 = 91;
data4 = 90;


let average5Decimal = checkAverage(data1, data2, data3, data4);
	console.log("checkAverage(" + data1 + "," + data2 + "," + data3 + "," + data4 + ")");
	let average5 = Math.round(average5Decimal);
console.log(average5);


if(average5 <= 74){
	console.log("Hello,student, your average is " + average5 + ". The Letter equivalent is F.");
}

else if(average5 >= 75 && average5 <= 79){
	console.log("Hello,student, your average is " + average5 + ". The Letter equivalent is D.");
}

else if(average5 >= 80 && average5 <= 84){
	console.log("Hello,student, your average is " + average5 + ". The Letter equivalent is C.");
}

else if(average5 >= 85 && average5 <= 89){
	console.log("Hello,student, your average is " + average5 + ". The Letter equivalent is B.");
}

else if(average5 >= 90 && average5 <= 95){
	console.log("Hello,student, your average is " + average5 + ". The Letter equivalent is A.");
}

else if(average5 > 96){
	console.log("Hello,student, your average is " + average5 + ". The Letter equivalent is A+.");
}


//(---------------------------------------------------)


function checkAverage(data1, data2, data3, data4){
	let solveAverage1 = (data1 + data2 + data3 + data4) / 4;
		
	return solveAverage1;

}
data1 = 91;
data2 = 96;
data3 = 97;
data4 = 95;


let average6Decimal = checkAverage(data1, data2, data3, data4);
	console.log("checkAverage(" + data1 + "," + data2 + "," + data3 + "," + data4 + ")");
	let average6 = Math.round(average6Decimal);
console.log(average6);


if(average6 <= 74){
	console.log("Hello,student, your average is " + average6 + ". The Letter equivalent is F.");
}

else if(average6 >= 75 && average6 <= 79){
	console.log("Hello,student, your average is " + average6 + ". The Letter equivalent is D.");
}

else if(average6 >= 80 && average6 <= 84){
	console.log("Hello,student, your average is " + average6 + ". The Letter equivalent is C.");
}

else if(average6 >= 85 && average6 <= 89){
	console.log("Hello,student, your average is " + average6 + ". The Letter equivalent is B.");
}

else if(average6 >= 90 && average6 <= 95){
	console.log("Hello,student, your average is " + average6 + ". The Letter equivalent is A.");
}

else if(average6 > 96){
	console.log("Hello,student, your average is " + average6 + ". The Letter equivalent is A+.");
}